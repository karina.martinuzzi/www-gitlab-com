### Ops as Code
There have been four major trends impacting operations; they are all essential to enabling Developer style workflows for operational tasks by defining those activities as code (or [data](http://radar.oreilly.com/2013/08/the-rise-of-infrastructure-as-data.html)).
They are Pipelines as Code, [Infrastructure as Code (IAC)](/topics/gitops/infrastructure-as-code/), Observability as Code (OAC) and Policy as Code. At GitLab, our roots in SCM make us especially well suited for supporting these trends.

#### Pipelines as Code

[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) is based on pipelines as code principles and a key competitive advantage against other tools that require manual configuration of pipelines. [Standardizing on common CI/CD pipeline tools has been proven to increase business results](https://www.mckinsey.com/industries/technology-media-and-telecommunications/our-insights/developer-velocity-at-work-key-lessons-from-industry-digital-leaders#). The [value of defining pipelines as code (i)](https://drive.google.com/file/d/1HuRjVqH4D8qEuWkhv77XT6PEnnpml2gn/view?usp=sharing) is evident:

* It enables developer control of their pipelines
* It allows versioning of pipeline changes and collaboration on any proposed changes
* It allows for consistency via re-usability of pipelines and jobs across multiple projects

GitLab will continue to lean into our pipelines as code capabilities by adding support for releases, advanced deployments and complex builds directly in your `.gitlab-ci.yml` files. We'll also add more support for sharing common pipeline code across groups to create a consistent CI/CD platform within your organization.

#### Infrastructure as Code
[IAC](https://en.wikipedia.org/wiki/Infrastructure_as_code) provides DevOps teams with a method to maintain consistency with the infrastructure their applications run on.
The best practices defined in IAC prevent configuration drift, and allow teams to maintain consistent performance and security characteristics without the need for ongoing manual intervention. 

IAC was made possible by the availability of rapidly deployed cloud infrastructure (IaaS platforms), and the corresponding buildup of tools to enable generic usage and consistent deployment to that infrastructure (Container Management Platforms).

With our roots as a source code management system, we will build on our existing workflows for code management, and [extend our ability to define and manage infrastructure configuration in your project repositories](/direction/delivery/infrastructure_as_code/) so you can achieve reliability and consistency in your application.

#### Observability and Operations as Code
Observability is a measure of your application which defines how well you can understand the state of your production system from its external outputs.
The more observable your application is, the more reliably you can operate it, both manually and through automation.
Our vision is to allow you to define your observability and operational automation in code, alongside your application code.
Whether that is the configuration of dashboards, metrics, alerts, runbooks, automation scripts or incident issues - GitLab will source-control those configurations and ensure they are reliably deployed to your production systems.

#### Policy as Code

Policy as Code is the idea that requirements for auditing and compliance teams can be collected with zero additional cognitive load from developers and other team members participating in software delivery. Because GitLab has an end-to-end view of the artifacts that are associated with releases (issues, merge requests, feature flags, and so on) we are uniquely positioned to provide a comprehensive view of compliance-related activity, collected on an ongoing basis throughout the build, test, and release process. Our vision here can be found in our [strategy page for Release Evidence](/direction/release/release_evidence/).
